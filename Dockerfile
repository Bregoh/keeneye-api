# the node app and version of node js
FROM node:12
LABEL developer="bright"

# set work directory
WORKDIR /usr/src/keeneye
# copy package.js
COPY package*.json ./
# install dependencies
RUN npm install

COPY . .

EXPOSE 4001

CMD ["npm", "start"]